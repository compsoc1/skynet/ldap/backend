use skynet_ldap_backend::{
  db_init, get_config,
  methods::{account_new, account_recover, account_ssh, account_update},
  State,
};

#[async_std::main]
async fn main() -> tide::Result<()> {
  let config = get_config();
  let db = db_init(&config).await?;

  let host_port = config.host_port.clone();

  tide::log::start();

  let state = State {
    db,
    config,
  };

  let mut app = tide::with_state(state);

  // for users to update their own profile
  app.at("/ldap/update").post(account_update::submit);

  // for new users
  app.at("/ldap/new/email").post(account_new::email::submit);
  app.at("/ldap/new/account").post(account_new::account::submit);

  // for folks who forget password/username
  app.at("/ldap/recover/password").post(account_recover::password::reset);
  app.at("/ldap/recover/password/auth").post(account_recover::password::auth);
  app.at("/ldap/recover/username").post(account_recover::username::submit);
  app.at("/ldap/recover/ssh/request").post(account_recover::ssh::request);
  app.at("/ldap/recover/ssh/verify").post(account_recover::ssh::verify);

  //for getting current ssh keys associated with the account
  app.at("/ldap/ssh").post(account_ssh::get_ssh_keys);
  app.at("/ldap/ssh").delete(account_ssh::remove_ssh_key);
  app.at("/ldap/ssh/add").post(account_ssh::add_ssh_key);

  app.listen(host_port).await?;
  Ok(())
}
