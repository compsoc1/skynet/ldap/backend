use ldap3::{LdapConn, Scope, SearchEntry};
use serde::{Deserialize, Serialize};
use skynet_ldap_backend::{db_init, get_config, AccountWolves, Accounts, Config};
use sqlx::{Pool, Sqlite};

#[async_std::main]
async fn main() -> tide::Result<()> {
  let config = get_config();
  let db = db_init(&config).await.unwrap();

  update_wolves(&config, &db).await;
  update_ldap(&config, &db).await;

  Ok(())
}

async fn update_wolves(config: &Config, db: &Pool<Sqlite>) {
  for account in get_wolves(config).await {
    update_account(db, &account).await;
  }
}

async fn update_ldap(config: &Config, db: &Pool<Sqlite>) {
  let mut ldap = match LdapConn::new(&config.ldap_host) {
    Ok(s) => s,
    Err(e) => {
      println!("{:?}", e);
      return;
    }
  };

  match ldap.simple_bind(&config.ldap_admin, &config.ldap_admin_pw) {
    Ok(_) => {}
    Err(e) => {
      println!("{:?}", e);
      return;
    }
  }

  // use this to pre load a large chunk of data
  if let Ok(x) = ldap.search(
    "ou=users,dc=skynet,dc=ie",
    Scope::OneLevel,
    "(objectClass=*)",
    vec!["uid", "uidNumber", "skDiscord", "skMemberOf", "mail", "skID", "userPassword"],
  ) {
    if let Ok((rs, _res)) = x.success() {
      for entry in rs {
        let tmp = SearchEntry::construct(entry);

        let mut tmp_account = Accounts {
          user: "".to_string(),
          uid: 0,
          mail: "".to_string(),
          student_id: "".to_string(),
          secure: false,
        };

        // pull out the required info
        if tmp.attrs.contains_key("uid") && !tmp.attrs["uid"].is_empty() {
          tmp_account.user = tmp.attrs["uid"][0].clone();
        }
        if tmp.attrs.contains_key("uidNumber") && !tmp.attrs["uidNumber"].is_empty() {
          tmp_account.uid = tmp.attrs["uidNumber"][0].clone().parse().unwrap_or(0);
        }
        if tmp.attrs.contains_key("mail") && !tmp.attrs["mail"].is_empty() {
          tmp_account.mail = tmp.attrs["mail"][0].clone();
        }
        if tmp.attrs.contains_key("skID") && !tmp.attrs["skID"].is_empty() {
          tmp_account.student_id = tmp.attrs["skID"][0].clone();
        }
        if tmp.attrs.contains_key("userPassword") && !tmp.attrs["userPassword"].is_empty() {
          tmp_account.secure = tmp.attrs["userPassword"][0].starts_with("{SSHA512}")
        }

        if !tmp_account.user.is_empty() {
          sqlx::query_as::<_, Accounts>(
            "
            INSERT OR REPLACE INTO accounts (user, uid, mail, student_id, secure)
            VALUES (?1, ?2, ?3, ?4, ?5)
            ",
          )
          .bind(&tmp_account.user)
          .bind(tmp_account.uid)
          .bind(&tmp_account.mail)
          .bind(&tmp_account.student_id)
          .bind(tmp_account.secure)
          .fetch_optional(db)
          .await
          .ok();
        }
      }
    }
  }

  // done with ldap
  ldap.unbind().unwrap();
}
impl From<&WolvesResultUser> for AccountWolves {
  fn from(input: &WolvesResultUser) -> Self {
    AccountWolves {
      id_wolves: input.wolves_id.parse::<i64>().unwrap_or(0),
      id_student: input.student_id.to_owned(),
      email: input.contact_email.to_owned(),
      expiry: input.expiry.to_owned(),
      name_first: Some(input.first_name.to_owned()),
      name_second: Some(input.last_name.to_owned()),
    }
  }
}

#[derive(Deserialize, Serialize, Debug)]
struct WolvesResultUser {
  committee: String,
  wolves_id: String,
  first_name: String,
  last_name: String,
  contact_email: String,
  student_id: Option<String>,
  note: Option<String>,
  expiry: String,
  requested: String,
  approved: String,
  sitename: String,
  domain: String,
}
#[derive(Deserialize, Serialize, Debug)]
struct WolvesResult {
  success: i8,
  result: Vec<WolvesResultUser>,
}

async fn get_wolves(config: &Config) -> Vec<AccountWolves> {
  if config.wolves_key.is_empty() {
    return vec![];
  }
  if config.wolves_url.is_empty() {
    return vec![];
  }

  // get wolves data
  if let Ok(mut res) = surf::post(&config.wolves_url).header("X-AM-Identity", &config.wolves_key).await {
    match res.body_json().await {
      Ok(WolvesResult {
        success,
        result,
      }) => {
        if success != 1 {
          return vec![];
        }
        return result.iter().map(AccountWolves::from).collect::<Vec<AccountWolves>>();
      }
      Err(e) => {
        println!("{:?}", e);
      }
    }
  }

  vec![]
}

async fn update_account(db: &Pool<Sqlite>, account: &AccountWolves) {
  sqlx::query_as::<_, AccountWolves>(
    "
    INSERT OR REPLACE INTO accounts_wolves (id_wolves, id_student, email, expiry, name_first, name_second)
    VALUES (?1, ?2, ?3, ?4, ?5, ?6)
    ",
  )
  .bind(account.id_wolves)
  .bind(&account.id_student)
  .bind(&account.email)
  .bind(&account.expiry)
  .bind(&account.name_first)
  .bind(&account.name_second)
  .fetch_optional(db)
  .await
  .ok();
}
