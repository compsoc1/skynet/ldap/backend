use crate::{get_now_iso, random_string, uid_to_dn, Accounts, AccountsReset, Config, State};
use chrono::{Duration, SecondsFormat, Utc};
use ldap3::{exop::PasswordModify, LdapConn};
use lettre::{
  message::{header, MultiPart, SinglePart},
  transport::smtp::{authentication::Credentials, response::Response, Error},
  Message, SmtpTransport, Transport,
};
use maud::html;
use sqlx::{Pool, Sqlite};
use tide::{
  prelude::{json, Deserialize},
  Request,
};

pub mod password {
  use super::*;

  #[derive(Debug, Deserialize)]
  struct PassReset {
    user: Option<String>,
    email: Option<String>,
  }

  /// Handles password resets
  /// All responses are success, never want to leak info
  pub async fn reset(mut req: Request<State>) -> tide::Result {
    let PassReset {
      user,
      email,
    } = req.body_json().await?;

    // check that any mail is not using @skynet.ie
    if let Some(mail) = &email {
      if mail.trim().ends_with("@skynet.ie") {
        // all responses from this are a success
        return Ok(json!({"result": "error", "error": "Skynet email not permitted."}).into());
      }
    }

    let config = &req.state().config;
    let db = &req.state().db;

    // considering the local db is updated hourly (or less) use that instead of teh ldap for lookups
    let user_details = match db_get_user(db, &user, &email).await {
      None => {
        return Ok(json!({"result": "success"}).into());
      }
      Some(x) => x,
    };

    let mail_is_skynet = user_details.mail.trim().ends_with("@skynet.ie");

    // user does not have a different email address set
    if mail_is_skynet && &user_details.student_id == "00000000" {
      // not returning an error here as there is no need to let the person requesting what email the user has
      return Ok(json!({"result": "success"}).into());
    }

    let mail = if mail_is_skynet {
      format!("{}@studentmail.ul.ie", &user_details.student_id)
    } else {
      user_details.mail
    };

    // check if a recent password reset request happened lately
    db_pending_clear_expired(db).await?;

    if db_get_user_reset(db, &user_details.user).await.is_some() {
      // reset already requested within timeframe
      return Ok(json!({"result": "success"}).into());
    }

    // send mail
    let auth = random_string(50);

    match send_mail(config, &user_details.user, &mail, &auth) {
      Ok(_) => {
        save_to_db(db, &user_details.user, &auth).await?;
      }
      Err(e) => {
        println!("{:?}", e);
      }
    }

    Ok(json!({"result": "success"}).into())
  }

  #[derive(Debug, Deserialize)]
  pub struct PassResetAuth {
    auth: String,
    pass: String,
  }

  pub async fn auth(mut req: Request<State>) -> tide::Result {
    let PassResetAuth {
      auth,
      pass,
    } = req.body_json().await?;

    let config = &req.state().config;
    let db = &req.state().db;

    if db_pending_clear_expired(db).await.is_err() {
      return Ok(json!({"result": "error1"}).into());
    }

    // check if auth exists
    let details = match db_get_user_reset_auth(db, &auth).await {
      None => {
        return Ok(json!({"result": "error2"}).into());
      }
      Some(x) => x,
    };

    if ldap_reset_pw(config, &details, &pass).await.is_err() {
      return Ok(json!({"result": "error", "error": "ldap error"}).into());
    };

    Ok(json!({"result": "success", "success": "Password set"}).into())
  }

  pub async fn db_get_user(pool: &Pool<Sqlite>, user_in: &Option<String>, mail_in: &Option<String>) -> Option<Accounts> {
    let user = match user_in {
      None => "",
      Some(x) => x,
    };
    let mail = match mail_in {
      None => "",
      Some(x) => x,
    };

    if let Ok(res) = sqlx::query_as::<_, Accounts>(
      r#"
        SELECT *
        FROM accounts
        WHERE user == ? OR mail ==?
        "#,
    )
    .bind(user)
    .bind(mail)
    .fetch_all(pool)
    .await
    {
      if !res.is_empty() {
        return Some(res[0].to_owned());
      }
    }

    None
  }

  async fn db_pending_clear_expired(pool: &Pool<Sqlite>) -> Result<Vec<AccountsReset>, sqlx::Error> {
    sqlx::query_as::<_, AccountsReset>(
      r#"
        DELETE 
        FROM accounts_reset
        WHERE date_expiry < ?
        "#,
    )
    .bind(get_now_iso(false))
    .fetch_all(pool)
    .await
  }

  async fn db_get_user_reset(pool: &Pool<Sqlite>, user: &str) -> Option<AccountsReset> {
    if let Ok(res) = sqlx::query_as::<_, AccountsReset>(
      r#"
        SELECT *
        FROM accounts_reset
        WHERE user == ?
        "#,
    )
    .bind(user)
    .fetch_all(pool)
    .await
    {
      if !res.is_empty() {
        return Some(res[0].to_owned());
      }
    }

    None
  }

  async fn db_get_user_reset_auth(pool: &Pool<Sqlite>, auth: &str) -> Option<AccountsReset> {
    if let Ok(res) = sqlx::query_as::<_, AccountsReset>(
      r#"
        SELECT *
        FROM accounts_reset
        WHERE auth_code == ?
        "#,
    )
    .bind(auth)
    .fetch_all(pool)
    .await
    {
      if !res.is_empty() {
        return Some(res[0].to_owned());
      }
    }

    None
  }

  async fn ldap_reset_pw(config: &Config, details: &AccountsReset, pass: &str) -> Result<(), ldap3::LdapError> {
    let mut ldap = LdapConn::new(&config.ldap_host)?;
    ldap.simple_bind(&config.ldap_admin, &config.ldap_admin_pw)?.success()?;

    let dn = uid_to_dn(&details.user);

    // if so then set password
    let tmp = PasswordModify {
      // none as we are staying on the same connection.
      user_id: Some(&dn),
      old_pass: None,
      new_pass: Some(pass),
    };

    ldap.extended(tmp)?.success()?;
    ldap.unbind()?;

    Ok(())
  }

  fn send_mail(config: &Config, recipient: &str, mail: &str, auth: &str) -> Result<Response, Error> {
    let url_base = "https://account.skynet.ie";
    let link_new = format!("{url_base}/recovery/password_reset?auth={auth}");
    let discord = "https://discord.skynet.ie";
    let sender = format!("UL Computer Society <{}>", &config.mail_user);

    // Create the html we want to send.
    let html = html! {
      head {
        title { "Hello from Skynet!" }
        style type="text/css" {
          "h2, h4 { font-family: Arial, Helvetica, sans-serif; }"
        }
      }
      div style="display: flex; flex-direction: column; align-items: center;" {
        h2 { "Hello from Skynet!" }
        // Substitute in the name of our recipient.
        p { "Hi " (recipient) "," }
        p {
          "Here is your password reset link:"
          br;
          a href=(link_new) { (link_new) }
        }
        p {
          "If did not request this please ignore."
        }
        p {
          "UL Computer Society"
          br;
          "Skynet Team"
          br;
          a href=(discord) { (discord) }
        }
      }
    };

    let body_text = format!(
      r#"
      Hi {recipient}
      
      Here is your password reset link:
      {link_new}
      
      If did not request this please ignore.
      
      UL Computer Society
      Skynet Team
      {discord}
      "#
    );

    // Build the message.
    let email = Message::builder()
      .from(sender.parse().unwrap())
      .to(mail.parse().unwrap())
      .subject("Skynet: Password Reset")
      .multipart(
        // This is composed of two parts.
        // also helps not trip spam settings (uneven number of url's
        MultiPart::alternative()
          .singlepart(SinglePart::builder().header(header::ContentType::TEXT_PLAIN).body(body_text))
          .singlepart(SinglePart::builder().header(header::ContentType::TEXT_HTML).body(html.into_string())),
      )
      .expect("failed to build email");

    let creds = Credentials::new(config.mail_user.clone(), config.mail_pass.clone());

    // Open a remote connection to gmail using STARTTLS
    let mailer = SmtpTransport::starttls_relay(&config.mail_smtp).unwrap().credentials(creds).build();

    // Send the email
    mailer.send(&email)
  }

  async fn save_to_db(db: &Pool<Sqlite>, user: &str, auth: &str) -> Result<Option<AccountsReset>, sqlx::Error> {
    // lets start off a 4 hour timeout on password resets
    let offset = Utc::now() + Duration::hours(4);

    sqlx::query_as::<_, AccountsReset>(
      "
        INSERT OR REPLACE INTO accounts_reset (user, auth_code, date_expiry)
        VALUES (?1, ?2, ?3)
        ",
    )
    .bind(user.to_owned())
    .bind(auth.to_owned())
    .bind(offset.to_rfc3339_opts(SecondsFormat::Millis, true))
    .fetch_optional(db)
    .await
  }
}

pub mod username {
  use super::password::db_get_user;
  use super::*;

  // far simpler, accept email, send notification via email

  #[derive(Debug, Deserialize)]
  struct UsernameReminder {
    email: String,
  }

  pub async fn submit(mut req: Request<State>) -> tide::Result {
    let UsernameReminder {
      email,
    } = req.body_json().await?;

    // check that any mail is not using @skynet.ie

    if email.trim().ends_with("@skynet.ie") {
      // all responses from this are a success
      return Ok(json!({"result": "error", "error": "Skynet email not permitted."}).into());
    }

    let config = &req.state().config;
    let db = &req.state().db;

    // considering the local db is updated hourly (or less) use that instead of teh ldap for lookups
    let user_details = match db_get_user(db, &None, &Some(email)).await {
      None => {
        return Ok(json!({"result": "success"}).into());
      }
      Some(x) => x,
    };

    // user does not have a different email address set
    if user_details.mail.trim().ends_with("@skynet.ie") {
      // not returning an error here as there is no need to let the person requesting what email the user has
      return Ok(json!({"result": "success"}).into());
    }

    send_mail(config, &user_details).ok();

    Ok(json!({"result": "success"}).into())
  }

  fn send_mail(config: &Config, record: &Accounts) -> Result<Response, Error> {
    let recipient = &record.user;
    let mail = &record.mail;
    let discord = "https://discord.skynet.ie";
    let sender = format!("UL Computer Society <{}>", &config.mail_user);

    // Create the html we want to send.
    let html = html! {
      head {
        title { "Hello from Skynet!" }
        style type="text/css" {
          "h2, h4 { font-family: Arial, Helvetica, sans-serif; }"
        }
      }
      div style="display: flex; flex-direction: column; align-items: center;" {
        h2 { "Hello from Skynet!" }
        // Substitute in the name of our recipient.
        p { "Hi there," }
        p {
          "You requested a username reminder: " (recipient)
        }
        p {
          "If did not request this please ignore."
        }
        p {
          "UL Computer Society"
          br;
          "Skynet Team"
          br;
          a href=(discord) { (discord) }
        }
      }
    };

    let body_text = format!(
      r#"
      Hi there,
      
      You requested a username reminder: {recipient}
      
      If did not request this please ignore.
      
      UL Computer Society
      Skynet Team
      {discord}
      "#
    );

    // Build the message.
    let email = Message::builder()
      .from(sender.parse().unwrap())
      .to(mail.parse().unwrap())
      .subject("Skynet: Username Reminder")
      .multipart(
        // This is composed of two parts.
        // also helps not trip spam settings (uneven number of url's
        MultiPart::alternative()
          .singlepart(SinglePart::builder().header(header::ContentType::TEXT_PLAIN).body(body_text))
          .singlepart(SinglePart::builder().header(header::ContentType::TEXT_HTML).body(html.into_string())),
      )
      .expect("failed to build email");

    let creds = Credentials::new(config.mail_user.clone(), config.mail_pass.clone());

    // Open a remote connection to gmail using STARTTLS
    let mailer = SmtpTransport::starttls_relay(&config.mail_smtp).unwrap().credentials(creds).build();

    // Send the email
    mailer.send(&email)
  }
}

pub mod ssh {
  use super::*;
  use crate::AccountsSSH;
  use ldap3::Mod;
  use ssh_key::{AuthorizedKeys, SshSig};
  use std::collections::HashSet;
  use std::fs;

  // this is for a legacy member who has forgotten their account password to be able to set an email.
  // With an email they can do a password recovery

  #[derive(Debug, Deserialize)]
  struct RequestChallenge {
    user: String,
    email: String,
  }

  pub async fn request(mut req: Request<State>) -> tide::Result {
    let RequestChallenge {
      user,
      email,
    } = req.body_json().await?;

    // check that any mail is not using @skynet.ie
    if email.trim().ends_with("@skynet.ie") {
      // all responses from this are a success
      return Ok(json!({"result": "error", "error": "Skynet email not permitted."}).into());
    }

    let config = &req.state().config;

    // check if <root>/<user>/.ssh/authorized_keys exists
    let path = format!("{}/{}/{}/.ssh/authorized_keys", &config.home, &config.ssh_root, user);
    let mut keys = vec![];
    if fs::read_to_string(&path).is_ok() {
      if let Ok(x) = AuthorizedKeys::read_file(path) {
        for entry in x {
          if let Ok(y) = entry.public_key().to_openssh() {
            keys.push(y);
          }
        }
      }
    }

    if keys.is_empty() {
      return Ok(json!({ "result": "success", "success": {  "auth": "", "keys": keys }}).into());
    }

    let db = &req.state().db;

    // check if there is ane listing entry, use that auth if exists
    if let Ok(result) = sqlx::query_as::<_, AccountsSSH>(
      r#"
      SELECT *
      FROM accounts_ssh
      WHERE user == ?
      "#,
    )
    .bind(&user)
    .fetch_one(db)
    .await
    {
      return Ok(json!({ "result": "success", "success": {  "auth": result.auth_code, "keys": keys }}).into());
    }

    // not in db, generate auth and save
    let auth = random_string(50);
    if sqlx::query_as::<_, AccountsSSH>(
      "
      INSERT OR REPLACE INTO accounts_ssh (user, auth_code, email)
      VALUES (?1, ?2, ?3)
      ",
    )
    .bind(&user)
    .bind(&auth)
    .bind(&email)
    .fetch_optional(db)
    .await
    .is_err()
    {
      // dont return any keys
      return Ok(json!({ "result": "success", "success": {  "auth": "", "keys": [] }}).into());
    }

    // return the full thing
    Ok(json!({"result": "success","success": { "auth": auth,"keys": keys }}).into())
  }

  #[derive(Debug, Deserialize)]
  struct RequestVerify {
    user: String,
    auth_signed: String,
  }

  // echo "auth code" | ssh-keygen -Y sign -n file -f /path/to/key
  pub async fn verify(mut req: Request<State>) -> tide::Result {
    let RequestVerify {
      user,
      auth_signed,
    } = req.body_json().await?;

    let db = &req.state().db;
    let config = &req.state().config;
    let details = if let Ok(result) = sqlx::query_as::<_, AccountsSSH>(
      r#"
      SELECT *
      FROM accounts_ssh
      WHERE user == ?
      "#,
    )
    .bind(&user)
    .fetch_one(db)
    .await
    {
      result
    } else {
      return Ok(json!({ "result": "error"}).into());
    };

    // check if <root>/<user>/.ssh/authorized_keys exists
    //let root = "/skynet_old";
    let path = format!("{}/{}/{}/.ssh/authorized_keys", &config.home, &config.ssh_root, user);

    let sig = match SshSig::from_pem(auth_signed) {
      Ok(x) => x,
      Err(_) => {
        return Ok(json!({ "result": "error", "error": "Incorrect signed format"}).into());
      }
    };

    // when ye echo it adds a newline to the end of the "file", need to duplicate it here.
    let msg_tmp = format!("{}\n", details.auth_code);
    let msg = msg_tmp.as_bytes();

    let mut valid = false;
    if fs::read_to_string(&path).is_ok() {
      if let Ok(x) = AuthorizedKeys::read_file(path) {
        for entry in x {
          let key = entry.public_key();
          if key.verify("file", msg, &sig).is_ok() {
            valid = true;
            break;
          }
        }
      }
    }

    if !valid {
      return Ok(json!({"result": "error", "error": "no valid key"}).into());
    }

    // add to ldap
    let mut ldap = LdapConn::new(&config.ldap_host)?;
    ldap.simple_bind(&config.ldap_admin, &config.ldap_admin_pw)?.success()?;

    let mods = vec![Mod::Replace(String::from("mail"), HashSet::from([details.email]))];

    let dn = format!("uid={},ou=users,dc=skynet,dc=ie", &user);
    ldap.modify(&dn, mods)?.success()?;
    ldap.unbind()?;

    // delete from tmp
    sqlx::query_as::<_, AccountsSSH>(
      r#"
      DELETE FROM accounts_ssh
      WHERE user == ?
      "#,
    )
    .bind(&user)
    .fetch_optional(db)
    .await?;

    // return the full thing
    Ok(json!({"result": "success", "success": "key valid"}).into())
  }
}
