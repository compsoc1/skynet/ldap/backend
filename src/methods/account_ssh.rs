use crate::{LdapAuth, LdapAuthResult, State};
use ldap3::{LdapConn, Mod, Scope, SearchEntry};
use std::collections::HashSet;
use tide::{
  prelude::{json, Deserialize},
  Request,
};

#[derive(Debug, Deserialize)]
struct SSHKey {
  auth: LdapAuth,
  key: String,
}

pub async fn add_ssh_key(mut req: Request<State>) -> tide::Result {
  let SSHKey {
    auth,
    key,
  } = req.body_json().await?;
  let config = &req.state().config;

  let LdapAuthResult {
    mut ldap,
    dn,
    ..
  } = match crate::auth_user(&auth, config).await {
    None => return Ok(json!({"result": "error", "error": "Failed to authenticate"}).into()),
    Some(x) => x,
  };

  let mods = vec![Mod::Add("sshPublicKey".to_owned(), HashSet::from([key]))];
  let result = match ldap.modify(&dn, mods) {
    Ok(_) => Ok(json!({"result": "success", "success": get_keys(&mut ldap, &dn) }).into()),
    Err(e) => {
      dbg!(e);
      Ok(json!({"result": "error", "error": "Failed to add key"}).into())
    }
  };

  ldap.unbind()?;

  result
}

pub async fn remove_ssh_key(mut req: Request<State>) -> tide::Result {
  let SSHKey {
    auth,
    key,
  } = req.body_json().await?;
  let config = &req.state().config;

  let LdapAuthResult {
    mut ldap,
    dn,
    ..
  } = match crate::auth_user(&auth, config).await {
    None => return Ok(json!({"result": "error", "error": "Failed to authenticate"}).into()),
    Some(x) => x,
  };

  let mods = vec![Mod::Delete("sshPublicKey".to_owned(), HashSet::from([key]))];

  let result = match ldap.modify(&dn, mods) {
    Ok(_) => Ok(json!({"result": "success", "success": get_keys(&mut ldap, &dn) }).into()),
    Err(e) => {
      dbg!(e);
      Ok(json!({"result": "error", "error": "Failed to remove key"}).into())
    }
  };

  ldap.unbind()?;

  result
}

#[derive(Debug, Deserialize)]
struct SSHKeyGet {
  auth: LdapAuth,
}

pub async fn get_ssh_keys(mut req: Request<State>) -> tide::Result {
  let SSHKeyGet {
    auth,
  } = req.body_json().await?;
  let config = &req.state().config;

  let LdapAuthResult {
    mut ldap,
    dn,
    ..
  } = match crate::auth_user(&auth, config).await {
    None => return Ok(json!({"result": "error", "error": "Failed to authenticate"}).into()),
    Some(x) => x,
  };

  let keys = get_keys(&mut ldap, &dn);

  ldap.unbind()?;

  Ok(json!({"result": "success", "success": keys}).into())
}

fn get_keys(ldap: &mut LdapConn, dn: &str) -> Vec<String> {
  let mut keys = vec![];

  if let Ok(result_tmp) = ldap.search(dn, Scope::Base, "(objectClass=*)", vec!["sshPublicKey"]) {
    if let Ok((rs, _)) = result_tmp.success() {
      for entry in rs {
        let tmp = SearchEntry::construct(entry);
        if tmp.attrs.contains_key("sshPublicKey") {
          for key in tmp.attrs["sshPublicKey"].clone() {
            keys.push(key);
          }
        }
      }
    }
  }

  keys
}
