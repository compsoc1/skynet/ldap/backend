use crate::{methods::account_new::email::get_wolves_mail, update_group, Accounts, Config, LdapAuth, LdapAuthResult, State};
use ldap3::{exop::PasswordModify, Mod};
use sqlx::{Pool, Sqlite};
use std::collections::HashSet;
use tide::{
  prelude::{json, Deserialize, Serialize},
  Request,
};

#[derive(Debug, Deserialize)]
pub struct LdapUpdate {
  auth: LdapAuth,
  field: String,
  value: String,
}

#[derive(Debug, Serialize)]
pub struct ModifyResult {
  mail: Option<String>,
  #[serde(rename = "sshPublicKey")]
  ssh_public_key: Option<String>,
  cn: Option<String>,
}

/// Handles updating a single field with the users own password
pub async fn submit(mut req: Request<State>) -> tide::Result {
  let LdapUpdate {
    auth,
    field,
    value,
  } = req.body_json().await?;

  // check that any mail is not using @skynet.ie
  if field == "mail" && value.trim().ends_with("@skynet.ie") {
    return Ok(json!({"result": "error", "error": "Skynet email not valid contact address"}).into());
  }

  let config = &req.state().config;
  let db = &req.state().db;

  // easier to give each request its own connection
  let LdapAuthResult {
    mut ldap,
    dn,
    is_skynet_user,
  } = match crate::auth_user(&auth, config).await {
    None => return Ok(json!({"result": "error", "error": "Failed to authenticate"}).into()),
    Some(x) => x,
  };

  // check if the password field itself is being updated
  if &field != "userPassword" {
    if !is_skynet_user && &field == "mail" {
      activate_group(db, config, &auth.user, &value).await;
    }

    // if password is not being updated then just update the required field
    let mods = vec![
      // the value we are updating
      Mod::Replace(field.clone(), HashSet::from([value.clone()])),
    ];

    ldap.modify(&dn, mods)?.success()?;
  } else {
    let tmp = PasswordModify {
      // none as we are staying on the same connection
      user_id: None,
      old_pass: Some(&auth.pass),
      new_pass: Some(&value),
    };

    ldap.extended(tmp)?.success()?;
  };

  ldap.unbind()?;

  // if its mail update the local db
  // here in case it fails above
  if &field == "mail" {
    update_local_db(db, "mail", &value).await.ok();
  }

  Ok(json!({"result": "success"}).into())
}

async fn activate_group(db: &Pool<Sqlite>, config: &Config, user: &str, mail: &str) {
  // check if user has this mail in teh wolves db
  if !get_wolves_mail(db, mail).await.is_empty() {
    // if so then activate
    if let Err(e) = update_group(config, "skynet-users", &vec![user.to_string()], false).await {
      println!("Couldnt add {} to skynet-users: {:?}", user, e)
    }
  }
}

async fn update_local_db(db: &Pool<Sqlite>, field: &str, value: &str) -> Result<Option<Accounts>, sqlx::Error> {
  let query = format!("INSERT OR REPLACE INTO accounts ({field}) VALUES (?1)");
  sqlx::query_as::<_, Accounts>(&query).bind(value.to_owned()).fetch_optional(db).await
}
