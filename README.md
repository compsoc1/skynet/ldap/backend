# Skynet LDAP backend

Basic information on the Skynet ldap backend for use on <https://account.skynet.ie>


### Error: HTTP 500  
No indication of the issue is returned to teh frontend.

## Routes
Base URL: <https://api.account.skynet.ie>


### POST /ldap/update
Fields:
* userPassword
* mail
* sshPublicKey
* cn
* sn
* skDiscord

```json
{ 
  "user" : "username", 
  "pass" : "password",
  "field": "field to change",
  "value": "new value of field"
}
```

Success:  
Each value is either a string or ``null``.
```json
{
  "result": "success",
  "success": {
    "cn": "Firstname Surname",
    "mail": "Email address",
    "skDiscord": null, 
    "sshPublicKey": "ssh key"
  }
}
```

Changing ``userPassword`` requires the existing password in teh apssword field and the new one in teh value field.

### POST /ldap/new/email

Kickstarts teh process of signing up to Skynet

```json
{ 
  "email" : "User's wolves email"
}
```


### POST /ldap/new/account

Verifies teh user has access to this email

```json
{ 
  "auth" : "Authcode from the email",
  "user" : "username the user wants", 
  "pass" : "password teh user wants"
}
```

Username taken:
```json
{"result": "error", "error": "username not available"}
```

Invalid Auth:
```json
{"result": "error", "error": "Invalid auth"}
```

## Responses

Generic responses which is used unless otherwise specified above.

### POST /ldap/recover/username
Sends an email to the user of the address reminding them of their username (if there is an account associated with said username).
```json
{
  "email" : "email looking for remidner"
}
```

### POST /ldap/recover/password

```json
{
  "user" : "[OPTIONAL] username looking for reset",
  "email" : "[OPTIONAL] email looking for reset"
}
```

All responses:
```json
{"result": "success"}
```

### POST /ldap/recover/password/auth

```json
{
  "auth" : "Auth key from teh email",
  "pass" : "Password the user chooses"
}
```

Early Errors:
```json
{"result": "error"}
```

LDAP error:
```json
{"result": "error", "error": "ldap error"}
```

Success:
```json
{"result": "success", "success": "Password set"}
```

### POST /ldap/recover/ssh/request
This endpoint can set emails for old skynet members who previously logged in via ssh keys.  
1. Request for their account.  
2. A list of public keys will be displayed.    
3. Sign using one of the private keys.
4. Submit the pem value.

```bash
echo "auth code" | ssh-keygen -Y sign -n file -f /path/to/private/key
```

```json
{ 
  "user" : "Skynet username",
  "email" : "Email to set"
}
```

Errors:
```json
{"result": "error", "error": "Skynet email not permitted."}
```

```json
{ "result": "success", "success": {  "auth": "", "keys": [] }}
```
```json
{ "result": "success", "success": {  "auth": "code here", "keys": ["key 1", "key 2"] }}
```

### POST /ldap/recover/ssh/verify


```json
{ 
  "user" : "Skynet username",
  "auth_signed" : "signed auth code"
}
```

#### Errors
```json
{ "result": "error"}
```

```json
{ "result": "error", "error": "Incorrect signed format"}
```

```json
{"result": "error", "error": "no valid key"}
```
#### Success

```json
{"result": "success", "success": "key valid"}
```

### POST /ldap/ssh
Returns array of SSH keys associated with the Skynet account
```json
{
  "auth" : {
    "user": "username",
    "pass": "password"
  }
}
```
#### Errors
```json
{"result": "error", "error": "Failed to authenticate"}
```
#### Success
```json
{"result": "success", "success": ["key1","key2","key3"]}
```

### DELETE /ldap/ssh
Deletes SSH key from Skynet account
```json
{
  "auth" : {
    "user": "username",
    "pass": "password"
  },
  "key": "ssh key"
}
```
#### Errors
```json
{"result": "error", "error": "Failed to authenticate"}
```
```json
{"result": "error", "error": "Failed to remove key"}
```
#### Success
```json
{"result": "success"}
```

### POST /ldap/ssh/add
Adds SSH key to Skynet account
```json
{
  "auth" : {
    "user": "username",
    "pass": "password"
  },
  "key": "ssh key"
}
```
#### Errors
```json
{"result": "error", "error": "Failed to authenticate"}
```
```json
{"result": "error", "error": "Failed to add key"}
```
#### Success
```json
{"result": "success"}
```
## Responses

Generic responses which is used unless otherwise specified above.


### Success: HTTP 200
```json
{
    "result": "success"
}
```
