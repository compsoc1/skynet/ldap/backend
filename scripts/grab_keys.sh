#!/bin/bash

from="./skynet_old/home"
to="./home_tmp"

find $from -maxdepth 3  -name "authorized_keys" -type f > tmp2
while IFS= read -r path_old
do
  echo "From: $path_old"
  path_new="${path_old/"$from"/"$to"}"
  echo "To:   $path_new"
  path_dir="${path_new/authorized_keys/}"
  mkdir -p "$path_dir"
  cp "$path_old" "$path_new"
done < tmp2
rm tmp2

chmod -R skynet_ldap_backend $to